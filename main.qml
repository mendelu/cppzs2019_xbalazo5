import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 1000
    height: 600
    title: qsTr("Tank")
    color: "grey"

    property var moving: false

    Rectangle {
      anchors.fill: parent
      focus: true

      Panel{
        id: rightpanel
      }

      LeftPanel{
        id: leftpanel
      }

      Field{
        id: field

        anchors {
          right: rightpanel.left
          left: leftpanel.right
          top: parent.top
          bottom: parent.bottom
        }
      }

      Timer {
        interval: 50;
        running: true;
        repeat: true;
        onTriggered: {
          if (moving) {
            if (field.direction == Qt.Key_Left) tan.moveLeft();
            else if (field.direction == Qt.Key_Right) tan.moveRight();
            else if (field.direction == Qt.Key_Up) tan.moveUp();
            else if (field.direction == Qt.Key_Down) tan.moveDown();
          }
          cpp.frame();
          //console.log(cpp.qshots)
         //console.log(cpp.qshots[0])
        }
      }

      Keys.onPressed: {
        if ([Qt.Key_Left, Qt.Key_Right, Qt.Key_Up, Qt.Key_Down].indexOf(event.key) > -1) {
          field.direction = event.key; moving = true;
        }
        if (event.key === Qt.Key_Space) {
            cpp.shoot(field.direction);
        }
      }

      Keys.onReleased: {
        if (event.key === field.direction) {
          moving = false;
        }
      }
    }
}
