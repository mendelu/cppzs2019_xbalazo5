#ifndef BULLET_H
#define BULLET_H

#include <QObject>
#include <QVariantList>
#include "enemy.h"


#define WIDTH 11
#define HEIGHT 11


class Bullet: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList bulletPosition READ getBulletPosition NOTIFY bulletChanged)

public:
    explicit Bullet( double x, double y, int direction, QObject *parent = nullptr);
    const QVariantList getBulletPosition() const;
    bool isAtEnd();
    double getX();
    double getY();


signals:
   void bulletChanged();


public slots:
    void move();

private:
    double bulletx, bullety;
    int direction;

};

Q_DECLARE_METATYPE(Bullet*)

#endif // BULLET_H
