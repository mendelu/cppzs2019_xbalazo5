#ifndef FILEREADER_H
#define FILEREADER_H

#include <QFile>
#include <QVariantList>
#include <QXmlStreamReader>
#include <QDebug>
#include "enemy.h"
#include <vector>

class Filereader
{
public:
    Filereader();
    static QVariantList loadFromXml();
    static void loadMap(QString mapName, std::vector<int> &fields, QList<QObject*> &enemies,int &playerx, int &playery);
};

#endif // FILEREADER_H
