import QtQuick 2.0

Rectangle {
    property alias buttonText:buttonLabel.text

    id: button
    color: "grey"
    height: 40
    width: 100
    radius: 2
    border.color: "red"
    border.width: 0

    Text {
        id: buttonLabel
        text: qsTr("Quit")
        anchors.centerIn: parent
        font.pixelSize: 28
        font.bold: true
        color: "white"
    }

    MouseArea{
        anchors.fill: parent
        hoverEnabled: true

        onEntered: {
            button.border.width = 3
            buttonLabel.color = "red"
        }

        onExited: {
            button.border.width = 0
            buttonLabel.color = "white"
        }
    }
}

