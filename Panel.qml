import QtQuick 2.0

Rectangle {
    property int margin: 20

    height: parent.height
    width: 200
    anchors.right: parent.right
    color: "black"

    Column{
        height: parent.height-margin
        anchors.centerIn: parent
        spacing: margin


        Button{
            id: level1
            buttonText: "Level 1"
            signal qmlLevel1()
            MouseArea{
                anchors.fill: parent

                onClicked: {
                    level1.qmlLevel()
                }
            }
        }

        Button{
            buttonText: "Level 2"
            id: level2
            signal qmlLevel2()
            MouseArea{
                anchors.fill: parent

                onClicked: {
                    level2.qmlLevel2()
                }
            }
        }

        Button {
            buttonText: "Level 3"
            id: level3
            signal qmlLevel3()
            MouseArea{
                anchors.fill: parent

                onClicked: {
                    level3.qmlLevel3()
                }
            }
        }


        Button{
            MouseArea{
                anchors.fill: parent

                onClicked: {
                    Qt.quit()
                }
            }
        }


    }
}
