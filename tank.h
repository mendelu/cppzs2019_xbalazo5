#ifndef TANK_H
#define TANK_H

#include <QObject>
#include <QVariantList>
#include <QDebug>
#include <unistd.h>


//#include"App.h"
#include "bullet.h"
#include "enemy.h"
#include "filereader.h"

#define WIDTH 11
#define HEIGHT 11
typedef int myMap[WIDTH][HEIGHT];

class Tank : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList tankPosition READ getTankPosition NOTIFY tankChanged)
public:
    Tank(QList<QObject*>& enemies, QObject *parent = nullptr);
    const QVariantList getTankPosition() const;
    bool isAtWall(double x, double y);
    double getX();
    double getY();
    void createEnemies();
    bool isEnemyAt(double x, double y);
    void setPos(double x, double y);
    void setWalls(QVariantList walls);

     Q_INVOKABLE Bullet* shoot(int direction);
     Q_INVOKABLE void moveLeft();
     Q_INVOKABLE void moveRight();
     Q_INVOKABLE void moveUp();
     Q_INVOKABLE void moveDown();
     //Q_INVOKABLE void shoot(int direction);
     Q_INVOKABLE int getHealth();


signals:
    void tankChanged();


private:
    double tankx = 5, tanky = 9;
    int m_health = 3;
    int m_walls[WIDTH][HEIGHT];

    QVariantList q_walls;
    QList<QObject*>& qenemies;
    // App &steny;

public slots:


};

#endif // TANK_H
