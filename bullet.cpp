#include "bullet.h"
#include <QTimer>
#include <QDebug>
#include <iostream>



Bullet::Bullet(double x, double y, int dir, QObject *parent)
  : QObject(parent), bulletx(x), bullety(y), direction(dir)
{
    qDebug() << "bullet created";

}

const QVariantList Bullet::getBulletPosition() const {
  QVariantList bullet;
  bullet << bulletx << bullety;
  return bullet;
}

double Bullet::getX(){
    return bulletx;
}

double Bullet::getY(){
    return bullety;
}

void Bullet::move(){
    if(direction== Qt::Key_Up){


        if(this->bullety - 0.1 >= 0)
        {
            this->bullety= this->bullety-0.10;
            bulletChanged();}
        }

    if(direction== Qt::Key_Down){

        if(this->bullety + 0.1 + 0.1 <= HEIGHT)
        {
             this->bullety= this->bullety+0.10;
             bulletChanged();}
    }

    if(direction== Qt::Key_Left){
        if(this->bulletx -0.1 >= 0){
            this->bulletx= this->bulletx-0.10;
            bulletChanged();
        }
    }

    if(direction== Qt::Key_Right){
        if(this->bulletx + 0.1 + 0.1 <= WIDTH){
    this->bulletx= this->bulletx+0.10;

    bulletChanged();}}
}

bool Bullet::isAtEnd(){
    if((bulletx-0.1<=0)||(bullety-0.1<=0)||(bulletx+0.2>=WIDTH)||(bullety+0.2>=HEIGHT)){
        return true;
    } else return false;
}


