#include "App.h"

#include <iostream>

#define MOVE 0.1
#define TANK_SIZE 0.8

// load walls from file
App::App(QObject *parent) {
  q_walls = Filereader::loadFromXml();
  QVariantList list;


  int i = 0;
  for (int y = 0; y < HEIGHT; y++) {
    for (int x = 0; x < WIDTH; x++) {
      m_walls[x][y] = q_walls[i].toBool();
      i++;
    }
  }

  createEnemies();
  this->tank = new Tank(this->enemies);
  std::cout<<" halo"<< std::endl;
  changeToLevel1();

}

void App::destroyEnemy(int x, int y){
    int pos = 0;
    for(auto enemy:enemies){
        bool isWantedEnemy = ((Enemy *)enemy)->getX() == x && ((Enemy *)enemy)->getY() == y;
        if (isWantedEnemy){
            enemies.removeAt(pos);
            enemiesChanged();
        }
        pos++;
    }
}





void App::createEnemies(){
    enemies.append(new Enemy(3,4));
    enemies.append(new Enemy(7,5));
    enemies.append(new Enemy(1,8));
    enemiesChanged();

    for(auto enemy:enemies){
        std::cout << "Enemy in App: " << ((Enemy *)enemy)->getX()
                  << ", " <<((Enemy *)enemy)->getY() << ";" << std::endl;
    }
}

// levels
void  App::changeToLevel1(){
    setNewMap("map.xml");
}
void  App::changeToLevel2(){
    setNewMap("map2.xml");
}
void  App::changeToLevel3(){
    setNewMap("map3.xml");
}


void App::dumpEverything()
{
    enemies.clear();
    shots.clear();
    q_walls.clear();
}

void App::setNewMap(QString mapName){
    dumpEverything();

    std::vector<int> walls;
    int x,y;
    std::cout<< "100"<< std::endl;
    Filereader::loadMap(mapName,walls,enemies,x,y);
    std::cout << "101"<<std::endl;
    for (int j =0; j< walls.size();++j){
        q_walls << walls.at(j);
    }


    int i = 0;
    for (int y = 0; y < HEIGHT; y++) {
      for (int x = 0; x < WIDTH; x++) {
        m_walls[x][y] = walls[i];
        i++;
      }
    }
    tank->setPos(x,y);
    tank->setWalls(q_walls);


    wallsChanged();
    enemiesChanged();


    std::cout<< x << " " << y << std::endl;
    std::cout<< enemies.size() << std::endl;
    std::cout<< walls.size() << std::endl;
}



// shoot a bullet ... TODO: connect with Tank's shoot()
void App::shoot(int direction){
    //shots.append(new Bullet(tank->getX()+TANK_SIZE/2,tank->getY()+TANK_SIZE/2, direction));
    //qDebug() << "shots " << steny.shots.size();
    shots.append(tank->shoot(direction));
    shotsChanged();
}

// map getter - pack walls to vector
std::vector<bool> App::getWalls(){
    std::vector<bool> walls;
    for (int y = 0; y < HEIGHT; y++) {
      for (int x = 0; x < WIDTH; x++) {
        walls.push_back(m_walls[y][x]);
      }
    }
    return walls;
}

QList<QObject*>& App::getShots(){
    return shots;
}
QList<QObject*>& App::getEnemies(){
    return enemies;
}
Tank* App::getTank(){
    return tank;
}

int App::getWidth()const{
    return WIDTH;
}

int App::getHeight()const{
    return HEIGHT;
}


// not sure what for is this one..
const QVariantList & App::walls() {
  q_walls.clear();

  for (int y = 0; y < HEIGHT; y++) {
    for (int x = 0; x < WIDTH; x++) {
      q_walls << m_walls[x][y];
    }
  }
  return q_walls;
}

// single ingame frame
void App::frame() {
  for (QObject *s : shots) ((Bullet *)s)->move();   //move all the bullets
  checkShots();    // remove shots that hit the wall/edge or enemy

}

// check if shot hit wall
//bool App::shotAtWall(double x, double y){
//    if (m_walls[int(x)][int(y)]==true){
//    return true;
//    }
//    return false;
//}

// check if enemy is at x,y
bool App::isEnemyAt(double x, double y){
    bool answer =false;
    for(auto enemy:enemies){
        if(((Enemy *)enemy)->getX()==int(x) && ((Enemy *)enemy)->getY()==int(y)){
            answer = true;
        }
    }
    return answer;
}

// check if wall is at x,y
bool App::isWallAt(double x, double y) {
  if (m_walls[int(x)][int(y)]==true){
  return true;
  }
  return false;
}

// checking if shots hit sth
void App::checkShots(){
    int pos =0;
    for(QObject *shot: shots){
        bool atWall = isWallAt(((Bullet *)shot)->getX(),((Bullet *)shot)->getY()) ;
        bool enemyHit = isEnemyAt(((Bullet *)shot)->getX(),((Bullet *)shot)->getY());
        bool isAtEdge = ((Bullet *)shot)->isAtEnd();
        if(  isAtEdge || atWall ){
          shots.removeAt(pos);
          shotsChanged();
        } else if (enemyHit){
            destroyEnemy(int(((Bullet *)shot)->getX()),int(((Bullet *)shot)->getY()));
            shots.removeAt(pos);
            shotsChanged();
        }
        pos++;
    }
}



