#include "tank.h"
#include <QtDebug>
#include <iostream>
#define MOVE 0.1
#define TANK_SIZE 0.8

Tank::Tank(QList<QObject*> &enemies_, QObject *parent) : QObject(parent), qenemies(enemies_)
{
    q_walls = Filereader::loadFromXml();

    int i = 0;
    for (int y = 0; y < HEIGHT; y++) {
      for (int x = 0; x < WIDTH; x++) {
        m_walls[x][y] = q_walls[i].toBool();
        i++;
      }
    }

    // QList of enemies check
    for(auto enemy:qenemies){
        std::cout << "Enemy in tank: " << ((Enemy *)enemy)->getX()
                  << ", " <<((Enemy *)enemy)->getY() << ";" << std::endl;
    }


}


//
// M O V E M E N T
//
void Tank::moveLeft() {
  if (
    isAtWall(tankx - MOVE, tanky) ||
    isAtWall(tankx - MOVE, tanky + 0.5*TANK_SIZE) ||
    isAtWall(tankx - MOVE, tanky + TANK_SIZE) ||
    tankx - MOVE <= 0
    || isEnemyAt(tankx - MOVE, tanky) ||
       isEnemyAt(tankx - MOVE, tanky + 0.5*TANK_SIZE) ||
       isEnemyAt(tankx - MOVE, tanky + TANK_SIZE)
  )
    return;

  tankx -= MOVE;
  tankChanged();
}

void Tank::moveRight() {
  if (
    isAtWall(tankx + TANK_SIZE + MOVE, tanky) ||
    isAtWall(tankx + TANK_SIZE + MOVE, tanky + 0.5*TANK_SIZE) ||
    isAtWall(tankx + TANK_SIZE + MOVE, tanky + TANK_SIZE) ||
    tankx + TANK_SIZE + MOVE >= WIDTH
          || isEnemyAt(tankx + TANK_SIZE + MOVE, tanky) ||
             isEnemyAt(tankx + TANK_SIZE + MOVE, tanky + 0.5*TANK_SIZE) ||
             isEnemyAt(tankx + TANK_SIZE + MOVE, tanky + TANK_SIZE)
  )
    return;

  tankx += MOVE;
  tankChanged();
}
void Tank::moveUp() {
  if (
    isAtWall(tankx, tanky - MOVE) ||
    isAtWall(tankx + 0.5*TANK_SIZE, tanky - MOVE) ||
    isAtWall(tankx + TANK_SIZE, tanky - MOVE) ||
    tanky - MOVE <= 0
          || isEnemyAt(tankx, tanky - MOVE) ||
             isEnemyAt(tankx + 0.5*TANK_SIZE, tanky - MOVE) ||
             isEnemyAt(tankx + TANK_SIZE, tanky - MOVE)
  )
    return;

  tanky -= MOVE;
  tankChanged();
}

void Tank::moveDown() {
  if (
    isAtWall(tankx, tanky + TANK_SIZE + MOVE) ||
    isAtWall(tankx + 0.5*TANK_SIZE, tanky + TANK_SIZE + MOVE) ||
    isAtWall(tankx + TANK_SIZE, tanky + TANK_SIZE + MOVE) ||
    tanky + TANK_SIZE + MOVE >= HEIGHT
          || isEnemyAt(tankx, tanky + TANK_SIZE + MOVE) ||
             isEnemyAt(tankx + 0.5*TANK_SIZE, tanky + TANK_SIZE + MOVE) ||
             isEnemyAt(tankx + TANK_SIZE, tanky + TANK_SIZE + MOVE)
  )
    return;

  tanky += MOVE;
  tankChanged();
// qDebug() << tankx;
}



//void Tank::shoot(int direction){
//    steny.shots.append(new Bullet(tankx+TANK_SIZE/2,tanky+TANK_SIZE/2, direction));
//    qDebug() << "shots " << steny.shots.size();
//    steny.shotsChanged();
//}


// unused
Bullet* Tank::shoot(int direction){
    Bullet *bullet = new Bullet(tankx+TANK_SIZE/2,tanky+TANK_SIZE/2, direction);
    return bullet;

}


// some getters
double Tank::getX(){
    return tankx;
}

double Tank::getY(){
    return tanky;
}

const QVariantList Tank::getTankPosition() const {
  QVariantList tank;
  tank << tankx << tanky;
  return tank;
}

int Tank::getHealth(){
    return m_health;
}

// to check if wall is at x,y
bool Tank::isAtWall(double x, double y){
    if (m_walls[int(x)][int(y)]==true){
    return true;
    }
    return false;
}

// check if enemy is at x,y
bool Tank::isEnemyAt(double x, double y){
    bool answer =false;

    for(auto enemy:qenemies){
        if(((Enemy *)enemy)->getX()==int(x) && ((Enemy *)enemy)->getY()==int(y)){
            answer = true;
            double x =((Enemy *)enemy)->getX();
            double y = ((Enemy *)enemy)->getY();
            std::cout << "X: "<< x<< " Y: "<< y<<std::endl;
        }
    }
    return answer;
}

void Tank::setPos(double x, double y)
{
    this->tankx = x;
    this->tanky = y;
}

void Tank::setWalls(QVariantList walls)
{
    q_walls = walls;
    int i = 0;
    for (int y = 0; y < HEIGHT; y++) {
      for (int x = 0; x < WIDTH; x++) {
        m_walls[x][y] = q_walls[i].toBool();
        i++;
      }
    }
}


