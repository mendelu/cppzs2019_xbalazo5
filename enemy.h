#ifndef ENEMY_H
#define ENEMY_H


#include<QVariantList>
#include <QObject>

class Enemy : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList enemyPosition READ getEnemyPosition NOTIFY enemyChanged)
public:
    explicit Enemy(double x, double y,QObject *parent = nullptr);
    const QVariantList getEnemyPosition() const;
    bool isEnemy(double x, double y);
    double getX();
    double getY();

signals:
    void enemyChanged();

public slots:

private:
 double x = 7, y = 5;


};

#endif // ENEMY_H
