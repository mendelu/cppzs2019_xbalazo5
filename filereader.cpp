#include "filereader.h"
#include <iostream>

#define WIDTH 11
#define HEIGHT 11


Filereader::Filereader()
{

}

QVariantList Filereader::loadFromXml(){
     QVariantList m_field;

    QFile file(":/map.xml");
    if(file.open(QIODevice::ReadOnly)){
        QXmlStreamReader xmlReader;
        xmlReader.setDevice(&file);
        xmlReader.readNext();

        while (!xmlReader.isEndDocument()){
            if (xmlReader.isStartElement()){
                QString name = xmlReader.name().toString();
                if (name == "field"){

                    int mineCount = xmlReader.readElementText().toInt();
                    m_field << mineCount;
                }
            }
            xmlReader.readNext();
        }

    } else {
        qCritical() << "nejde otvorit subor\n";
    }
    return m_field;
}

void Filereader::loadMap(QString mapName,std::vector<int> &fields, QList<QObject*> &enemies, int &playerx, int &playery)
{
    QString fileName = ":/" + mapName;
    QFile file(fileName);     // TODO - finish
    if(file.open(QIODevice::ReadOnly)){
        QXmlStreamReader xmlReader;
        xmlReader.setDevice(&file);
        xmlReader.readNext();

        while (!xmlReader.isEndDocument()){
            if (xmlReader.isStartElement()){
                QString name = xmlReader.name().toString();
                if (name == "field"){
                    // fields
                    int field = xmlReader.readElementText().toInt();
                    fields.push_back(field);
                    //
                    }
                }
            xmlReader.readNext();
            }
        }

    // looking for enemies and tank pos
    int i = 0;
    for (int y = 0; y < HEIGHT; y++) {
      for (int x = 0; x < WIDTH; x++) {
        if(fields.at(i)==2){
            enemies.append(new Enemy(x,y));
            fields[i] = 0;
        } else if(fields.at(i)==3){
            playerx = x;
            playery = y;
            fields[i] = 0;
        }

        i++;
      }
    }


}
