import QtQuick 2.0

Rectangle {
    property alias cellText:cellLabel.text

    id: cell
    color: "red"
    height: 40
    width: 120
    radius: 2

    Text {
        property int margin: 5
        id: cellLabel

        text:  qsTr(" HP: ") + tan.getHealth()
        font.pixelSize: 28
        font.bold: true
        color: "light gray"
    }


}

