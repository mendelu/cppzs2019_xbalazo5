#ifndef APP_H
#define APP_H

#include <QObject>
#include <QVariantList>
#include <QDebug>
#include <QList>
#include <QQmlListProperty>
#include <unistd.h>
#include "filereader.h"
#include "bullet.h"
#include "enemy.h"
#include <vector>
#include "tank.h"


#define WIDTH 11
#define HEIGHT 11

class App : public QObject {
  Q_OBJECT
  Q_PROPERTY(QVariantList qwalls READ walls NOTIFY wallsChanged)
  Q_PROPERTY(QList<QObject*> qshots READ getShots NOTIFY shotsChanged)
  Q_PROPERTY(QList<QObject*> qenemy READ getEnemies NOTIFY enemiesChanged)
  Q_PROPERTY(int width READ getWidth)
  Q_PROPERTY(int height READ getHeight)


private:
  QVariantList q_walls;
  QList<QObject *> shots;
  QList<QObject *> enemies;
  int m_walls[WIDTH][HEIGHT];

  Tank *tank;



public:
  Q_INVOKABLE void shoot(int direction);
  Q_INVOKABLE void frame();


  explicit App(QObject *parent = nullptr);
  const QVariantList & walls();
  std::vector<bool> getWalls();
  int getWidth() const;
  int getHeight() const;
  bool isEnemyAt(double x, double y);
  bool shotAtWall(double x, double y);
  bool isWallAt(double x, double y);
  void checkShots();
  void createEnemies();
  void setNewMap(QString mapName);
  void dumpEverything();

  void destroyEnemy(int x, int y);
  virtual ~App() {};

  // int getShotCount(QQmlListProperty<Bullet *> *property) { return property.shots.size(); }
  // Bullet *getShotAt(QQmlListProperty<Bullet *> *property, int ix) { return shots[ix]; }


  QList<QObject*>& getShots();
  QList<QObject*>& getEnemies();
  Tank* getTank();

public slots:
  void  changeToLevel1();
  void  changeToLevel2();
  void  changeToLevel3();

signals:
  void wallsChanged();
  void shotsChanged();
  void enemiesChanged();


};

Q_DECLARE_METATYPE(QQmlListProperty<Bullet>)

#endif
