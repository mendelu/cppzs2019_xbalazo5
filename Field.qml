import QtQuick 2.0

Rectangle {

  id: field
  color: "white"


  property var direction: Qt.Key_Up
  readonly property int row_count: cpp.height



  Grid {
    id: grid
    columns: cpp.width

    Repeater {
      model: cpp.qwalls
      delegate: Rectangle {
        width: field.width / grid.columns
        height: field.height / field.row_count
        Image {
          anchors.fill: parent
          source: "images/walls3.jpg"
          visible: modelData
        }
      }
    }
  }

  Rectangle {
    id: tank

    color: "transparent"
    width: field.width / grid.columns * 0.8
    height: field.height / field.row_count * 0.8
    x: tan.tankPosition[0] * field.width / grid.columns
    y: tan.tankPosition[1] * field.height / field.row_count
    Image {
      anchors.fill: parent
      source: "images/skuska.png"
      transform: Rotation {
        origin.x: tank.width / 2
        origin.y: tank.height / 2
        angle: {
          if (field.direction == Qt.Key_Left) return -90
          else if (field.direction == Qt.Key_Right) return 90
          else if (field.direction == Qt.Key_Up) return 0
          else if (field.direction == Qt.Key_Down) return 180
        }
      }
    }
  }

  Repeater {
    model: cpp.qshots
    delegate: Rectangle {
      width: 5
      height: 5
      color: "red"
      x: modelData.bulletPosition[0] * field.width / grid.columns
      y: modelData.bulletPosition[1] * field.width / grid.columns

      /*Component.onCompleted: {
        console.log('fooooo');

      }*/
    }
  }

//  Rectangle {
//      id: enemy
//      color: "transparent"
//      width: field.width / grid.columns
//      height: field.height / field.row_count
//      x: ene.enemyPosition[0] * field.width / grid.columns
//      y: ene.enemyPosition[1] * field.height / field.row_count
//      Image{
//        anchors.fill: parent
//        source: "images/enemy.png"
//      }
//  }

  Repeater {
    model: cpp.qenemy
    delegate: Rectangle {
      width: field.width / grid.columns
      height: field.height / field.row_count
      x: modelData.enemyPosition[0] * field.width / grid.columns
      y: modelData.enemyPosition[1] * field.height / field.row_count
      Image {
          anchors.fill: parent
          source: "images/enemy.png"
      }
    }
  }

  // Rectangle{
    //   color: "transparent"
    //   width: parent.width<parent.height?parent.width:parent.height*0.025
    //   height: width
     //  Image{
      //   anchors.fill: parent
      //   source: "images/bullet-icon.jpg"
     //  }
  // }
}
