#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "App.h"
#include "enemy.h"
#include "tank.h"
#include "bullet.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    App *scene = new App();


    engine.rootContext()->setContextProperty("cpp", scene);
    engine.rootContext()->setContextProperty("tan", scene->getTank());
    //engine.rootContext()->setContextProperty("ene", &enemy);


    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

//    QObject *home = engine.rootObjects().first();
//    QObject::connect(home, SIGNAL(qmlLevel2()), scene, SLOT(changeToLevel2()));


    return app.exec();
}
