#include "enemy.h"

Enemy::Enemy(double x, double y,QObject *parent) : QObject(parent)
{
    this->x = x;
    this->y = y;
}



double Enemy::getX(){
    return x;
}

double Enemy::getY(){
    return y;
}

const QVariantList Enemy::getEnemyPosition() const {
  QVariantList enemy;
  enemy << x << y;
  return enemy;
}

bool Enemy::isEnemy(double x, double y) {
    if((x == int(x) ) and (y == int(y))){
        return true;
    }
  return false;
}
